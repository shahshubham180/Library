FROM adoptopenjdk/openjdk11:latest
VOLUME /tmp
ADD /target/*.jar Library-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/Library-0.0.1-SNAPSHOT.jar"]
