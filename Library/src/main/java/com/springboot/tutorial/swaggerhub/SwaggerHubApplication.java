package com.springboot.tutorial.swaggerhub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.client.RestTemplate;

import com.springboot.tutorial.swaggerhub.customer.utils.CustomerUtils;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "BOOK API", description = "Book Microservice"))
@EnableWebSecurity
public class SwaggerHubApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwaggerHubApplication.class, args);
	}
	
	@Bean
	public CustomerUtils getCustomerUtils() {
		return new CustomerUtils();
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
