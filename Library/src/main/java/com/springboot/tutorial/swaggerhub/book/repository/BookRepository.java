package com.springboot.tutorial.swaggerhub.book.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.tutorial.swaggerhub.book.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {

	public Book findByBookId(Long id);

	public Book getBookByBookName(String bookName);

}
