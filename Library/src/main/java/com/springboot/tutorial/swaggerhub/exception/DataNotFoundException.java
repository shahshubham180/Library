package com.springboot.tutorial.swaggerhub.exception;

public class DataNotFoundException extends Exception{

	public DataNotFoundException(String message) {
		super(message);
	}
}
