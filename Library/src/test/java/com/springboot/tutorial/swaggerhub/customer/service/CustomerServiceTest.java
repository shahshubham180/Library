package com.springboot.tutorial.swaggerhub.customer.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


import java.net.URISyntaxException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.springboot.tutorial.swaggerhub.book.entity.Book;
import com.springboot.tutorial.swaggerhub.book.repository.BookRepository;
import com.springboot.tutorial.swaggerhub.customer.entity.Customer;
import com.springboot.tutorial.swaggerhub.customer.repository.CustomerRepository;
import com.springboot.tutorial.swaggerhub.customer.utils.CustomerUtils;
import com.springboot.tutorial.swaggerhub.exception.BookIdAndNameMismatchException;
import com.springboot.tutorial.swaggerhub.exception.DataNotFoundException;




@SpringBootTest
public class CustomerServiceTest {

	@InjectMocks
	private CustomerService customerService;
	
	@Mock
	private BookRepository bookRepository;
	
	@Mock
	private CustomerRepository customerRepository;
	
	@Mock
	private CustomerUtils utils;
	
	@Mock
	private RestTemplate restTemplate;
	
	private Customer request;
	
	private Book bookResponse;
	
	private Customer response;
	
	private static final Logger log= LoggerFactory.getLogger(CustomerServiceTest.class);
	
	
	@BeforeEach
	public void loadData() {
		
		request = new Customer();
		request.setFirstName("Sam");
		request.setLastName("Michael");
		request.setAge(20l);
		request.setCustId(1l);
		request.setEmail("Sam.michael@gmail.com");
		request.setBookId(1l);
		request.setBookName("Angels & Demons");
		
		response = new Customer();
		response.setFirstName("Sam");
		response.setLastName("Michael");
		response.setAge(20l);
		response.setCustId(1l);
		response.setEmail("Sam.michael@gmail.com");
		response.setBookId(1l);
		response.setBookName("Angels & Demons");
		
		bookResponse = new Book();
		bookResponse.setAuthor("Dan Brown");
		bookResponse.setBookId(1l);
		bookResponse.setBookName("Angels & Demons");
		bookResponse.setPublisher("Pocket Books");
		
		ReflectionTestUtils.setField(customerService, "seniorAgeGroup", "Bhagvat Gita");
		ReflectionTestUtils.setField(customerService, "juniorAgeGroup", "Shiva Triology");
	}
	
	@AfterAll
	public static void testEnds() {
		log.info("Completed Test Execution for Class Customer Service");
	}
	
	@Test
	public void createCustomerAccountTest() throws BookIdAndNameMismatchException, DataNotFoundException{
		Mockito.when(bookRepository.getBookByBookName(Mockito.anyString())).thenReturn(bookResponse);
		Mockito.when(customerRepository.save(request)).thenReturn(response);
		Customer customer = customerService.createCustomerAccount(request);
		assertEquals("Sam", customer.getFirstName());
		assertEquals("Michael", customer.getLastName());
		assertEquals(20l, customer.getAge());
		assertEquals(1, customer.getCustId());
		assertEquals("Sam.michael@gmail.com", customer.getEmail());
		assertEquals(1l, customer.getBookId());
		assertEquals("Angels & Demons", customer.getBookName());
	}
	
	@Test
	public void createCustomerAccountWithDataNotFoundTest() {
		Mockito.when(bookRepository.getBookByBookName(Mockito.anyString())).thenReturn(null);
		DataNotFoundException thrown = Assertions.assertThrows(DataNotFoundException.class, ()->
			customerService.createCustomerAccount(request));
		assertEquals("Book is not registered. Please register the Book", thrown.getMessage());
	}
	
	@Test
	public void createCustomerAccountWithBookIdMismatchTest() {
		request.setBookId(2l);
		Mockito.when(bookRepository.getBookByBookName(Mockito.anyString())).thenReturn(bookResponse);
		BookIdAndNameMismatchException thrown = Assertions.assertThrows(BookIdAndNameMismatchException.class, ()->
			customerService.createCustomerAccount(request));
		assertEquals("Book Id is invalid for the Book Angels & Demons", thrown.getMessage());
	}
	
	@Test
	public void createCustomerAccountWithNoBookNameTest() {
		request.setBookName(null);
		DataNotFoundException thrown = Assertions.assertThrows(DataNotFoundException.class, ()->
			customerService.createCustomerAccount(request));
		assertEquals("The customer must register with a Book", thrown.getMessage());
	}
	
	@Test
	public void deleteCustomerTest() throws DataNotFoundException {
		Mockito.when(customerRepository.getCustomerByCustId(request.getCustId())).thenReturn(response);
		ArgumentCaptor<Customer> customerCaptor = ArgumentCaptor.forClass(Customer.class);
		doNothing().when(customerRepository).delete(customerCaptor.capture());
		customerService.deleteCustomer(request);
		verify(customerRepository, times(1)).delete(request);
		assertEquals("Sam", customerCaptor.getValue().getFirstName());
		assertEquals("Michael", customerCaptor.getValue().getLastName());
		assertEquals(20l, customerCaptor.getValue().getAge());
		assertEquals(1, customerCaptor.getValue().getCustId());
		assertEquals("Sam.michael@gmail.com", customerCaptor.getValue().getEmail());
		assertEquals(1l, customerCaptor.getValue().getBookId());
		assertEquals("Angels & Demons", customerCaptor.getValue().getBookName());
	}
	
	@Test
	public void deleteCustomerExceptionTest() throws DataNotFoundException {
		Mockito.when(customerRepository.getCustomerByCustId(request.getCustId())).thenReturn(null);
		DataNotFoundException thrown = Assertions.assertThrows(DataNotFoundException.class, ()->
		customerService.deleteCustomer(request));
		assertEquals("Customer Does Not Exist To Delete", thrown.getMessage());
	}
	
	@Test
	public void getCustomerInfoTest() throws DataNotFoundException {
		response.setLastName(null);
		Mockito.when(customerRepository.getCustomerByCustId(request.getCustId())).thenReturn(response);
		Customer customer = customerService.getCustomerInfo(request.getCustId());
		verify(utils, times(1)).createFullName(Mockito.anyString(), Mockito.isNull());
		assertEquals("Sam", customer.getFirstName());
		assertEquals(20l, customer.getAge());
		assertEquals(1, customer.getCustId());
		assertEquals("Sam.michael@gmail.com", customer.getEmail());
		assertEquals(1l, customer.getBookId());
		assertEquals("Angels & Demons", customer.getBookName());
		assertNull(customer.getLastName());
		
	}
	
	@Test()
	public void getCustomerExceptionTest() {
		Mockito.doThrow(NullPointerException.class).when(utils).createFullName(Mockito.anyString(), Mockito.anyString());
		Mockito.when(customerRepository.getCustomerByCustId(request.getCustId())).thenReturn(response);
		Assertions.assertThrows(NullPointerException.class, ()->customerService.getCustomerInfo(request.getCustId()));	
	}
	
	@Test
	public void getBookSuggestionTest() throws Exception {
		request.setBookName(null);
		Customer customer = customerService.bookSuggestion(request);
		assertEquals("Shiva Triology", customer.getBookName());
	}
	
	@Test
	public void getBookSuggestionPublicApiTest() throws Exception{
		request.setAge(null);
		request.setBookName(null);
		String jsonString = "{\"name\":\"michael\",\"age\":69,\"count\":233482}";
		Mockito.when(restTemplate.getForEntity(Mockito.anyString(), Mockito.any()))
			.thenReturn(new ResponseEntity<>(jsonString, HttpStatus.OK));
		Customer customer = customerService.bookSuggestion(request);
		assertEquals("Bhagvat Gita",customer.getBookName());
		assertEquals(69l, customer.getAge());
		
	}

}
